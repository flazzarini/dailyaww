from flask import current_app, Blueprint, send_file

latest = Blueprint('latest', __name__, url_prefix='/latest')


@latest.after_request
def after_request(response):
    """
    Add cors headers
    """
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    return response


@latest.route('/')
def get_latest_image():
    """
    Returns latest aww image to user
    """
    filename = current_app.cfg.get('reddit', 'destination_file')
    return send_file(filename, mimetype='image/png')
