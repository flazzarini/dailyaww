"""
This module contains the Redditor class, which is responsible to fetch
the latest aww picture
"""

import logging
from random import choice
from io import BytesIO
from typing import Optional, List

import requests
from praw import Reddit
from PIL import Image


LOG = logging.getLogger(__name__)


class Redditor:
    """
    Redditor can be used to fetch the latest top of the day /r/aww image
    """

    def __init__(
        self,
        client_id: str,
        client_secret: str,
        username: str,
        destination_file: str = './latest.png',
        subreddits: List[str] = ['dogpictures'],
        limit: int = 10
    ) -> None:

        self.client_id = client_id
        self.client_secret = client_secret
        self.username = username
        self.destination_file = destination_file
        self.subreddits = subreddits
        self.limit = limit

        self._client = None

    @property
    def user_agent(self) -> str:
        """Create User Agent String"""
        return "dailyaww /u/%s" % self.username

    @property
    def client(self) -> 'Reddit':
        """Gets a :py:class:`.Reddit` instance"""
        if not self._client:
            self._client = Reddit(
                client_id=self.client_id,
                client_secret=self.client_secret,
                user_agent=self.user_agent
            )
        return self._client

    def get_random_latest_image_url(self) -> Optional[str]:
        """Get latest random image url from random subreddit"""
        image_domains = [
            'i.redd.it',
            'i.imgur.com',
        ]
        submissions = []
        for subreddit in self.subreddits:
            for submission in self.client.subreddit(subreddit).top(
                    time_filter='day', limit=self.limit):

                if submission.domain in image_domains:
                    submissions.append(submission.url)

        LOG.info("Found %r valid submissions" % len(submissions))
        return choice(submissions)

    def save_latest(self) -> None:
        """Get latest random images and saves it to disk"""
        url = self.get_random_latest_image_url()
        if not url:
            return

        resp = requests.get(url)
        if resp.status_code != 200:
            LOG.error("Request to %r failed with error code %r",
                      url, resp.status_code)
            return

        bytescontent = BytesIO(resp.content)
        new_image = Image.open(bytescontent)
        new_image.save(self.destination_file)
        LOG.info("New image saved to %r" % self.destination_file)
