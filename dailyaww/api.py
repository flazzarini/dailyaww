"""
Main entry point for dailyaww application
"""

import os
import logging
from configparser import ConfigParser
from flask import Flask
from apscheduler.schedulers.background import BackgroundScheduler

from dailyaww.redditor import Redditor
from dailyaww.blueprints.latest import latest
from dailyaww.blueprints.root import root


LOG = logging.getLogger(__name__)


def get_redditor(config: ConfigParser) -> Redditor:
    """
    Creates an instance of :py:class:`.Redditor` using a Config instance
    """
    # Initiate a new Redditor instance
    sec = 'reddit'
    client_id = config.get(sec, 'client_id')
    client_secret = config.get(sec, 'client_secret')
    username = config.get(sec, 'username')
    destination_file = config.get(sec, 'destination_file')
    subreddits = [
        _.strip() for _ in config.get(
            sec, 'subreddits').split('\n') if _.strip()
    ]

    if any([not client_id, not client_secret, not username]):
        raise Exception(
            "Please make sure you have your reddit creds "
            "defined in your config"
        )

    return Redditor(
        client_id=client_id,
        client_secret=client_secret,
        username=username,
        destination_file=destination_file,
        subreddits=subreddits
    )


def setup_logging() -> None:
    """Void function that setups logging with gunicorn in mind"""
    gunicorn_logger = logging.getLogger('gunicorn.error')
    LOG.handlers = gunicorn_logger.handlers
    LOG.setLevel(gunicorn_logger.level)


def create_app() -> Flask:
    """
    Creates a new Flask application and returns it
    """
    setup_logging()

    # Read config
    config_filename = os.environ.get(
        'DAILYAWW_CONFIGFILE', './dailyaww-config.cfg'
    )

    if not config_filename:
        raise Exception("Please define DAILYAWW_CONFIGFILE env variable")

    config = ConfigParser()
    config.read(config_filename)

    redditor = get_redditor(config)
    redditor.save_latest()

    cron = BackgroundScheduler()

    @cron.scheduled_job('interval', hours=1)
    def _get_latest_aww_image():
        LOG.info("[CRON] Get latest aww image")
        redditor.save_latest()

    cron.start()

    app = Flask(__name__)
    app.cfg = config
    app.register_blueprint(root)
    app.register_blueprint(latest)
    LOG.info("Application is initialized")
    return app


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    APP = create_app()
    APP.run()
