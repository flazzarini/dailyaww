from os import listdir
from os.path import join

from fabric import task
from fabric.connection import Connection


def get_package_name(conn):
    """Return package name"""
    result = conn.run("env/bin/python setup.py --name")
    return result.stdout.strip("\n")


@task
def develop(conn):
    """Creates development environment"""
    conn.run("[ -d env ] || python3 -m venv env", replace_env=False)
    conn.run("env/bin/pip install -U pip setuptools", replace_env=False)
    conn.run("env/bin/pip install wheel", replace_env=False)
    conn.run("env/bin/pip install -e .[test,dev]", replace_env=False)


@task
def run(conn):
    """Run application"""
    conn.run(
        "DAILYAWW_CONFIGFILE=./config.cfg "
        "./env/bin/python dailyaww/api.py", replace_env=False)


@task
def build(conn):
    """Builds python package"""
    conn.run("./env/bin/python setup.py clean")
    conn.run("./env/bin/python setup.py bdist bdist_wheel")


@task
def publish(conn):
    """Publish to pyrepo"""
    conn.run("./env/bin/python setup.py clean")
    conn.run("./env/bin/python setup.py bdist bdist_wheel")
    filename = conn.run("python setup.py --fullname").stdout.strip("\n")

    repo_dir = "/var/www/gefoo.org/pyrepo/"
    remote = Connection('pyrepo.gefoo.org')
    remote.put("dist/%s-py3-none-any.whl" % filename, repo_dir)


@task
def docker_build(conn):
    """Builds docker image"""
    conn.run("./env/bin/python setup.py clean")
    conn.run("./env/bin/python setup.py bdist bdist_wheel")
    version = conn.run(
        "./env/bin/python setup.py --version").stdout.strip("\n")

    conn.run(
        "docker build "
        "--build-arg DAILYAWW_VERSION=%s "
        "-t flazzarini/dailyaww:latest ." % version)
    conn.run(
        "docker tag "
        "flazzarini/dailyaww:latest "
        "flazzarini/dailyaww:%s" % version
    )
    conn.run(
        "docker tag "
        "flazzarini/dailyaww:latest "
        "registry.gefoo.org/flazzarini/dailyaww:latest"
    )
    conn.run(
        "docker tag "
        "flazzarini/dailyaww:latest "
        "registry.gefoo.org/flazzarini/dailyaww:%s" % version
    )

    conn.run(
        "docker push registry.gefoo.org/flazzarini/dailyaww:latest")
    conn.run(
        "docker push registry.gefoo.org/flazzarini/dailyaww:%s" % version)


@task
def doc(conn):
    """Builds doc"""
    from os.path import abspath
    opts = {'builddir': '_build',
            'sphinx': abspath('env/bin/sphinx-build')}
    cmd = ('{sphinx} -b html '
           '-d {builddir}/doctrees . {builddir}/html')
    conn.local("cd doc && %s" % cmd.format(**opts))


@task
def test(conn):
    """Run tests"""
    conn.run(
        "env/bin/pytest-watch %s/ tests/ -- --lf --color yes" % (
            get_package_name(conn)
        )
    )


@task
def test_cov(conn):
    """Run tests with coverage checks"""
    conn.run(
        "env/bin/py.test --cov=%s --cov-report=html" % get_package_name(conn))
