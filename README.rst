Dailyaww
========

Retrieves latest top pictures from reddit.com subreddits. You can define
subreddits to be used in the config file. The path to the config file is read
from the environment variable `DAILYAWW_CONFIGFILE`.

Example config file::

    [reddit]
    client_id=<your_reddit_client_id>
    client_secret=<your_reddit_client_secret>
    username=<your_username>
    destination_file=<path_to_store_images>
    subreddits=
        dogpictures
        BostonTerrier
        rarepuppers
        lookatmydog



Installation
------------

To install using pip:

::

    $ pip install dailyaww


Docker
------

To run the docker image you will need to mount a valid config file inside the
conatiner to ``/dailyaww-config.cfg``.

::

    docker run -v myconfig.cfg:/dailyaww-config.cfg flazzarini/dailyaww
