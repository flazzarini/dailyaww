#
# dailyaww - Dockerfile
#
#  Provides a container able to run dailyaww
#
FROM python:3.7-alpine

MAINTAINER Frank Lazzarini <flazzarini@gmail.com>

ARG DAILYAWW_VERSION
ENV DAILYAWW_CONFIGFILE=/dailyaww-config.cfg

EXPOSE 8000

RUN mkdir /config
COPY dist/dailyaww-${DAILYAWW_VERSION}-py3-none-any.whl /tmp/
COPY config.cfg.dist /dailyaww-config.cfg

RUN apk --no-cache add \
    python3-dev \
    openssl \
    build-base \
    jpeg-dev \
    zlib-dev \
    freetype-dev \
    lcms2-dev \
    openjpeg-dev \
    tiff-dev \
    tk-dev \
    tcl-dev \
    harfbuzz-dev \
    fribidi-dev

RUN pip3 install /tmp/dailyaww-${DAILYAWW_VERSION}-py3-none-any.whl
RUN pip3 install gunicorn

CMD ["gunicorn", \
     "--log-level=debug", \
     "--bind", "0.0.0.0:8000", \
     "dailyaww.api:create_app()"]
