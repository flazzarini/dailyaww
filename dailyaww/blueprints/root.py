from flask import Blueprint, jsonify
from dailyaww import __version__

root = Blueprint('root', __name__, url_prefix='/')


@root.route('/')
def index():
    """
    Index page
    """
    return jsonify(
        {
            'dailyaww_version': "%s" % __version__
        }
    )
